<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Soulmate Bootstarp Website Template | Register :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
</head>
<body>
<?php
	$header = file_get_contents('header.php');
	echo $header;
?>
<div class="profile_banner">
	<div class="container"> 
	   <h3>Registrate</h3>
	</div>
</div>
<div class='profiles'>
  <div class="container">
	   <div class="register">
		  	  <form method="POST" action="control/registerprocess.php"> 
				 <div class="register-top-grid">
					<h3>Informacion personal</h3>
					 <div>
						<span>Nombre(s)<label>*</label></span>
						<input type="text" name="firstname" required> 
					 </div>
					 <div>
						<span>Apellido<label>*</label></span>
						<input type="text" name="lastname" required> 
					 </div>
					 <div>
						<span>Nickname<label>*</label></span>
						<input type="text" name="nickname" required> 
					 </div>
					 <div>
						 <span>e-mail<label>*</label></span>
						 <input type="email" name="email" required> 
					 </div>
					 <div>
						 <span>sexo<label>*</label></span>
						 <fieldset id="sex">
						 	<div>
						 		<p>hombre</p>
							 	<input type="radio" name="sex" value="0">	
						 	</div>
							<div>
								<p>mujer</p>
							 	<input type="radio" name="sex" value="1">
							</div>					 	
						 </fieldset>
					 </div>
					 <div>
						 <span>edad<label>*</label></span>
						 <input type="number" name="age" required> 
					 </div>
				     <div class="register-bottom-grid">
						    <h3>Información de cuenta</h3>
							 <div>
								<span>Contraseña<label>*</label></span>
								<input type="password" name="password" required>
							 </div>
							 <div>
								<span>Confirma Contraseña<label>*</label></span>
								<input type="password" name="confirmpassword" required>
							 </div>
							 <div class="clearfix"> </div>
					 </div>
				<div class="clearfix"> </div>
				<div class="register-but">
					   <input type="submit" value="Enviar">
					   <div class="clearfix"> </div>
				</div>
		   </form>
		</div>
		</div>
  </div>
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
</body>
</html>		
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<title>QPido</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
</head>
<body>
<?php
	$header = file_get_contents('header.php');
	echo $header;
?>
<div class="profile_banner">
	<div class="container"> 
	   <h3>Inicia sesión</h3>
	</div>
</div>
<div class='profiles'>
  <div class="container">
	  <div class="account_grid">
			 <div class="col-md-6 login-left">
			  	 <h3>¿Eres nuevo? ¡Crea una cuenta!</h3>
				 <p>Creando una cuenta para conocer a personas con gustos similares a los tuyos.</p>
				 <a class="acount-btn" href="register.php">Crear una cuenta</a>
			 </div>
			 <div class="col-md-6 login-right">
			  	<h3>Ya tengo una cuenta</h3>
				<p>Si ya tiene una cuenta, inicie sesión.</p>
				<form action="control/loginprocess.php" method="POST">
				  <div>
					<span>nickname<label>*</label></span>
					<input name="nickname" type="text"> 
				  </div>
				  <div>
					<span>Contraseña<label>*</label></span>
					<input name="pass" type="password"> 
				  </div>
				  <input type="submit" value="Entrar">
			    </form>
			 </div>	
			 <div class="clearfix"> </div>
	   </div>
  </div>
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
</body>
</html>		
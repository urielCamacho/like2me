<?php
	session_start();
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	require_once('pdo.php');
	if(isset($_POST['hobbie_name'])&&isset($_SESSION['nickname'])){
		$nickname = $_SESSION['nickname'];
		$hobbie = $_POST['hobbie_name'];
		$query = "SELECT * FROM hobbies WHERE name=:hobbie_name";
		$handler = $PDO->prepare($query);
		$handler->bindParam(':hobbie_name',$hobbie);
		$handler->execute();
		if($handler->rowCount()=='1'){
			//El hobbie ya existe, se agrega al otro.
			$idhobbie = $handler->fetch(PDO::FETCH_ASSOC);
			$idhobbie = $idhobbie['id_hobbie'];
			//Se inserta en users_hobbies
			$query = "INSERT INTO users_hobbies (id_hobbie,nickname) VALUES (:id_hobbie,:nickname)";
			$handler = $PDO->prepare($query);
			$handler->bindParam(':id_hobbie',$idhobbie);
			$handler->bindParam(':nickname',$nickname);
			$handler->execute();
			$msg = array('success' => true);
			echo json_encode($msg, JSON_FORCE_OBJECT);
			exit();
		}else{
			$query = "INSERT INTO hobbies (name) VALUES (:name)";
			$handler= $PDO->prepare($query);
			$handler->bindParam(':name',$hobbie);
			$handler->execute();
			//El hobbie ya existe, se agrega al otro.
			
			$query = "SELECT * FROM hobbies WHERE name=:hobbie_name";
			$handler = $PDO->prepare($query);
			$handler->bindParam(':hobbie_name',$hobbie);
			$handler->execute();
			if($handler->rowCount()!='1'){
				$msg = array('success'=>false, 'message'=>'Ha ocurrido un error');
				echo json_encode($msg, JSON_FORCE_OBJECT);
				exit();	
			} 
			//Se inserta en users_hobbies
			$idhobbie = $handler->fetch(PDO::FETCH_ASSOC);
			$idhobbie = $idhobbie['id_hobbie'];
			$query = "INSERT INTO users_hobbies (id_hobbie,nickname) VALUES (:id_hobbie,:nickname)";
			$handler = $PDO->prepare($query);
			$handler->bindParam(':id_hobbie',$idhobbie);
			$handler->bindParam(':nickname',$nickname);
			$handler->execute();
			$msg = array('success' =>true);
			echo json_encode($msg, JSON_FORCE_OBJECT);
		}
	}else{
		$msg = array('success' => false,'message'=>'Ha ocurrido un error');
		echo json_encode($msg, JSON_FORCE_OBJECT);
		//exit();
	}
?>
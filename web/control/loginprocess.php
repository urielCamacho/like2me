<?php
	session_start();
	require_once('pdo.php');
	error_reporting(E_ALL);
ini_set('display_errors', 1);
	if(!isset($_SESSION['nickname'])&&isset($_POST['pass'])&&isset($_POST['nickname'])){
		$pass = $_POST['pass'];
		$nickname = $_POST['nickname']; 	
		$query = "SELECT * FROM users WHERE nickname=:nickname AND password=:pass";
		$handler = $PDO->prepare($query);
		$handler->bindParam(':nickname',$nickname);
		$handler->bindParam(':pass',$pass);
		$handler->execute();
		if($handler->rowCount()=='1'){
			$user = $handler->fetch(PDO::FETCH_ASSOC);
			$_SESSION['nickname'] = $user['nickname'];
			header('Location: ../aboutme.php');
		}else{
			echo 'Usuario o pass incorrecto';
		}
	}else{
		if(!isset($_SESSION['nickname'])){
			header('Location: ../login.php');
		}else{
			header('Location: ../aboutme.php');
		}
	}
?>
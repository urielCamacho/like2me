<?php
	session_start();
	require_once('pdo.php');
	error_reporting(E_ALL);
	ini_set('display_errors', 1);
	if(isset($_POST['firstname'])&&isset($_POST['lastname'])&&isset($_POST['nickname'])&&isset($_POST['sex'])&&isset($_POST['password'])&&isset($_POST['email'])&&isset($_POST['age'])&&isset($_POST['confirmpassword'])){
		$firstname=htmlentities($_POST['firstname']);
		$lastname=htmlentities($_POST['lastname']);
		$email=htmlentities($_POST['email']);
		$nickname=htmlentities($_POST['nickname']);
		$sex=htmlentities($_POST['sex']);
		$age=htmlentities($_POST['age']);
		$password=htmlentities($_POST['password']);
		$confirmpassword=htmlentities($_POST['confirmpassword']);
		$type = '0';
		$query = "SELECT nickname FROM users WHERE nickname=:nickname";
		$handler = $PDO->prepare($query);
		$handler->bindParam(':nickname',$nickname);
		$handler->execute();
		if(strcmp($password, $confirmpassword)!=0){
			sleep(2);
			echo "<script>alert('La contrasena no coincide');</script>";
			header('Location: ../register.php');
			exit();
		}
		if($handler->rowCount()=='1'){
			echo "<script>alert('El usuario ya existe');</script>";
			sleep(2);
			header('Location: ../register.php');
		}else{
//			echo $firstname;
//			echo $lastname;
			$query = "INSERT INTO users (nickname,name,lastname, age, sex, password, email,type) VALUES (:nickname,:name,:lastname,:age,:sex,:password,:email,:type)";
			$handler=$PDO->prepare($query);
			$handler->bindParam(':nickname',$nickname);
			$handler->bindParam(':name',$firstname);
			$handler->bindParam(':lastname',$lastname);
			$handler->bindParam(':age',$age);
			$handler->bindParam(':sex',$sex);
			$handler->bindParam(':password',$password);
			$handler->bindParam(':email',$email);
			$handler->bindParam(':type',$type);
			$handler->execute();
			echo "<script>alert('Usuario creado exitosamente');</script>";
			header('Location: ../login.php');

		}

	}else{
		echo 'Algun campo esta vacio';
		sleep(2);
		header('Location: ../register.php');
	}
?>
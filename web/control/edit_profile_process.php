<?php
	//pdo.php required
	session_start();//start user's session
	require_once('pdo.php');
	//php file for edit our own profile
	//sended by /web/edit_profile.php
	//name, lastname, nickname, age, email_user, password, and confirmpassword
	//sended via POST method


	//We first verify that all our variables are set (Not null)
	if(isset($_POST['name'])&&isset($_POST['lastname'])&&isset($_POST['nickname'])&&isset($_POST['age'])&&isset($_POST['email_user'])&&isset($_POST['password'])&&isset($_POST['confirmpassword'])){
		//verify if our passwords match!
		if(strcmp($_POST['password'], $_POST['confirmpassword'])==0){
			$name = $_POST['name'];
			$lastname = $_POST['lastname'];
			$nickname = $_POST['nickname'];
			$age = $_POST['age'];
			$email_user = $_POST['email_user'];
			$password = $_POST['password'];
			$confirmpassword = $_POST['confirmpassword'];
			//echo 'Paswords matches';
			//updating in our db
			$query = "UPDATE users SET name=:name, lastname=:lastname, nickname=:nickname, age=:age, email=:email_user, password=:password WHERE nickname=:nickname";
			$handler = $PDO->prepare($query);
			$handler->bindParam(':name', $name);
			$handler->bindParam(':lastname', $lastname);
			$handler->bindParam(':nickname', $nickname);
			$handler->bindParam('age', $age);
			$handler->bindParam(':email_user', $email_user);
			$handler->bindParam(':password', $password);

			$handler->execute();
			header('Location: ../aboutme.php');
		}else{
			echo 'Las contraseñas no coinciden';
		}
	}else{
		echo 'Error, algun campo esta vacio';
	}
?>
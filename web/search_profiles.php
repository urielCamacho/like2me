<?php
	session_start();
	require_once(control/pdo.php);

	//function to search people
	function findpeople($sex, $age,$PDO){
		$handler=$PDO->prepare($query);
		$handler->bindParam(':sex',$sex);
		$handler->bindParam(':age',$age);

	}
?>

<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Soulmate Bootstarp Website Template | Add Profile :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
<!--Animation-->
<script src="js/wow.min.js"></script>
<link href="css/animate.css" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
</head>
<body>
<?php
	$header = file_get_contents('header_users.php');
	echo $header;
?>
<div class="profile_banner wow fadeInUpBig" data-wow-delay="0.4s">
	<div class="container"> 
	   <h3>Busca a personas con tus gustos</h3>
	</div>
</div>
<div class='profiles'>
	<div class="container">
		<form method="POST" action="search_profile_process.php">
			<div class="row">
			<div class="col-md-6">
				<div class="form-group">
					<h3 class="m_1 wow">Elige sexo</h3>
					<fieldset id="sex">
						<p class="m_3" style="width: 50%; float: left; text-align: center;">Hombre</p>
						<p class="m_3" style="width: 50%; float: left; text-align: center;">Mujer</p>
						<div class="btngpsex">
							<input type="radio" class="form-control" name="sex" value="0" style="width:50px; height:50px;">	
						</div>
						<div class="btngpsex">
							<input type="radio" class="form-control" name="sex" value="1" style="width:50px; height:50px;">
						</div>
					</fieldset>
				</div>
			</div>
			<div class="col-md-6 form-group">
				<h3 class="m_1 wow">Elige la edad minima de búsqueda.</h3>
				<input class="age_range form-control" type="range" name="age_people" min="18" max="70" step="1" value="18">
				<p class="range_year" style="font-size: 30px; float:right;">18</p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				
			</div>
			<div class="col-md-4">
				<div class="register-but">
					<input class="searchbtn" type="submit" value="Buscar">	
				</div>
				
			</div>
			<div class="col-md-4">
				
			</div>
		</div>	
		</form>
		
		<?php
			//if sex and age set, search!
			if(isset($_POST['sex'])&&isset($_POST['age_people'])){
				
			}
		?>
		<div class="about_box1">
	  	<h1 class="m_4 wow flipInX" data-wow-delay="0.4s">
        </h1>
       <div class="about_grid1">
          <div class="col-md-4 grid_1 wow lightSpeedIn" data-wow-delay="0.4s"> 
          	 <img src="images/pic5.jpg" class="img-responsive" alt=""/>
          	    <div class="btn-wrap bg_2">
                     <p class="text_2 bg_1 color_2">
                          <span></span>
                     </p>
					 <a href="#" class="text_3 color_3">
                         <span>More info</span>
                     </a>
                </div>
                <p class="m_9">Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore.</p>
          </div>
          <div class="clearfix"> </div>
       </div>
	</div>
	</div>
	
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
<script type="text/javascript" src="js/search.js"></script>
</body>
</html>		
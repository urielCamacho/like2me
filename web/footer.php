<div class="footer">
	<div class="container">
	    <div class="copy">
			<!--<p>&copy; 2014 Template by <a href="http://w3layouts.com" target="_blank">w3layouts</a></p>-->
			<p>&copy; <?php echo date("Y"); ?> Camacho, Uriel; Corona, Rafael; Marin, Daniel. <a href="http://w3layouts.com" target="_blank">Aplicaciones Web</a></p>
		</div>
		<div class="social"> 
			<ul class="footer_social">
			  <li><a href="#"> <i class="fb"> </i> </a></li>
			  <li><a href="#"> <i class="tw"> </i> </a></li>
		   </ul>
		</div>
	    <div class='clearfix'> </div>
	</div>
</div>
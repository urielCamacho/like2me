<?php
	session_start();
	require_once('control/pdo.php');
	//quitar la siguiente linea y este comentario para evitar confusion
	//$_SESSION['nickname']="uriel";
	function get_info($name,$PDO){
		$query = "SELECT * FROM users WHERE nickname=:nickname";
		$handler = $PDO->prepare($query);
		$handler->bindParam(":nickname",$name);
		$handler->execute();
		if($handler->rowCount()>0){
			$user = $handler->fetch(PDO::FETCH_ASSOC);
			return $user;
		}else{
			header('Location: login.php');
			exit();
		}
	}

	if(!isset($_SESSION['nickname'])){
		header('Location: login.php');
		exit();
	}
	$nickname = $_SESSION['nickname'];
	$user = get_info($nickname,$PDO);


?>

<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Soulmate Bootstarp Website Template | Single :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
</head>
<body>
<?php
	$header = file_get_contents('header_users.php');
	echo $header;
?>
<div class="profile_banner">
	<div class="container"> 
	   <h3>Mi Perfil</h3>
	</div>
</div>
<div class="profile">
	<div class="container">
		<div class="col-md-8 profile_left">
			<div class="lsidebar span_1_of_profile">
			  <div class="grid_4">
			    <img src="images/pic1.jpg" class="img-responsive" alt=""/>
				<ul class="category_desc1">
				   <li class="cate_right">
				   	 <ul class="list1">
				   	    <li><a href="#"><i class="heart"> </i></a></li>
				   	    <li><a href="#"><i class="heart"> </i></a></li>
				   	   
				   	 </ul>
				   </li>
				   <div class="clearfix"> </div>
				</ul>
			   </div>
			</div>
			<div class="cont span_2_of_profile">
				<h4><?php echo $user['name']." ".$user['lastname']; ?></h4>	
				<table class="profile-fields">
					<tbody>
						<tr>
							<th>Nickname</th>
							<td class="nickname"><?php echo $user['nickname'] ?></td>
						</tr>
						<tr>
							<th>Genero</th>
							<td class="sex"><?php 
								if($user['sex']=='0'){
									echo "Hombre";
								}else{
									echo "Mujer";
								}
							?></td>
						</tr>
												<tr>
							<th>Edad</th>
							<td class="age"><?php echo $user['age'] ?></td>
						</tr>			
						<tr>
							<th>e-mail</th>
							<td class="email"><?php echo $user['email'] ?></td>
						</tr>
					</tbody>
				</table>
		    </div>
			<div class="clearfix"></div>	
		</div>
	    <div class="col-md-4">
		    <h4 class="m_4"></h4>
			<a href="search_profiles.php" class="btn1 btn-1 btn1-1b">Encontrar personas</a>
		</div>

	    <div class="col-md-4">
		    <h4 class="m_4"></h4>
			<a href="edit_profile.php" class="editbtn btn2 btn-2 btn2-2b">Editar mi perfil</a>
		</div>
		<div class="col-md-4"></div>
		<div class="col-md-4"></div>
		<div class="col-md-4">
		    <h4 class="m_4"></h4>
			<a href="add_hobbies.php" class="editbtn btn2 btn-2 btn2-2b">Agregar Hobbies</a>
		</div>
	</div>
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
</body>
</html>		
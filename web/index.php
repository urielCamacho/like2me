<?php
	session_start();
	if(isset($_SESSION['nickname'])){
		header('Location: aboutme.php');
	}
?>
<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Soulmate Bootstarp Website Template | Home :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
<script src="js/jquery.easydropdown.js"></script>
<!-- FlexSlider -->
<link href="css/flexslider.css" rel='stylesheet' type='text/css' />
							  <script defer src="js/jquery.flexslider.js"></script>
							  <script type="text/javascript">
								$(function(){
								  SyntaxHighlighter.all();
								});
								$(window).load(function(){
								  $('.flexslider').flexslider({
									animation: "slide",
									start: function(slider){
									  $('body').removeClass('loading');
									}
								  });
								});
							  </script>
<!-- FlexSlider -->
<!--Animation-->
<script src="js/wow.min.js"></script>
<link href="css/animate.css" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
</head>
<body>
<?php 
	$header = file_get_contents('header.php');
	echo $header;
?>
<div class="banner wow bounceInRight" data-wow-delay="0.4s">
	<div class="container"> 
	   <div class="banner_right">
		   <h2>Encuentra a la persona ideal!</h2>
		   <h1>Busca personas con gustos similares a los tuyos.</h1>
		   <ul class="button">
			 <li><a href="login.php" class="btn1 btn2 btn-1 btn1-1b">Empezar</a></li>
		   </ul>
	   </div>
	</div>
</div>
<div class="content_top">
  <div class="container">
  	<div class="col-md-12 grid_1">
  		<h2>Recomendaciones musicales</h2>
  	</div>
  	<div class="row">
  		<div class="col-md-6 grid_1">
		  <iframe width="" height="" src="https://www.youtube.com/embed/6fHoMw8tCzo" frameborder="0" allowfullscreen></iframe>
		</div>
		<div class="col-md-6 grid_1">
		  <iframe width="" height="" src="https://www.youtube.com/embed/jtQgnfCK8Aw" frameborder="0" allowfullscreen></iframe>
		</div>
  	</div>
  </div>
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
</body>
</html>		
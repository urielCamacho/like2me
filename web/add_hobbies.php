<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Soulmate Bootstarp Website Template | Register :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
</head>
<body>
<?php
	session_start();
	require_once('control/pdo.php');
	$header = file_get_contents('header_users.php');
	echo $header;
?>
<div class="profile_banner">
	<div class="container"> 
	   <h3>Agrega tus gustos</h3>
	</div>
</div>
<div class='profiles'>
  <div class="container">
  	<div class="row wow">
  		<div class="col-md-6">
  			<div id="hobbies">
				<span>Agrega al menos 20 gustos<label>*</label></span>
				<input type="text" name="hobbie" name="hobbie-datalist" required="">
				<datalist for="hobbie-datalist"></datalist>
				<img src="images/add.png" name="add_hobbie">
			</div>
  		</div>
  		<div class="col-md-6">
  			<table style="width: 100%;">
  				<thead>
  					<th>Gustos</th>	
  				</thead>
  				<tbody class="hobbies_like">
  					<?php
  						if(isset($_SESSION['nickname'])){
  							$nickname = $_SESSION['nickname'];
  							$query = "SELECT * FROM users, hobbies, users_hobbies WHERE users.nickname=:nickname AND users.nickname = users_hobbies.nickname AND hobbies.id_hobbie=users_hobbies.id_hobbie";
  							$handler = $PDO->prepare($query);
  							$handler->bindParam(':nickname',$nickname);
  							$handler->execute();
  							if($handler->rowCount()>0){
  								$hobbies = $handler->fetchAll(PDO::FETCH_ASSOC);
  								for ($i=1; $i <=$handler->rowCount() ; $i++) { 
  									echo "<tr><td>".$hobbies[$i]['name']."</td></tr>";
  								}
  							}
  						}else{
  							header('Location: index.php');
  						}
  					?>
  				</tbody>
  			</table>
  		</div>
  	</div>
  	<a href="aboutme.php" class="btn1 btn-1 btn1-1b">Regresar</a>
  </div>
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
<script type="text/javascript" src="js/script_hobbies.js"></script>
</body>
</html>		
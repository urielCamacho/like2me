<!--A Design by W3layouts
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE HTML>
<html>
<head>
<title>Soulmate Bootstarp Website Template | Add Profile :: w3layouts</title>
<link href="css/bootstrap.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!--webfont-->
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<!--dropdown-->
<script src="js/jquery-1.11.1.min.js"></script>
<!--Animation-->
<script src="js/wow.min.js"></script>
<link href="css/animate.css" rel='stylesheet' type='text/css' />
<script>
	new WOW().init();
</script>
</head>
<body>
<?php
	$header = file_get_contents('header.php');
	echo $header;
?>
<div class="profile_banner wow fadeInUpBig" data-wow-delay="0.4s">
	<div class="container"> 
	   <h3>Modificar perfiles</h3>
	</div>
</div>
<div class='profiles'>
	<div class="container">
	  <div class="about_box1">
	  	<h1 class="m_4 wow flipInX" data-wow-delay="0.4s">
          Usuarios
        </h1>
        <p class="m_5 wow flipInY" data-wow-delay="0.4s">
           Edita o elimina perfiles
        </p>
       <div class="about_grid1">
          <div class="col-md-4 grid_1 wow lightSpeedIn" data-wow-delay="0.4s"> 
          	 <img src="images/pic5.jpg" class="img-responsive" alt=""/>
          	    <div class="btn-wrap bg_2">
                     <a href="#" class="text_3 color_3">
                         <span>Editar</span>
                     </a>
					 <a href="#" class="text_3 color_3 deleteuser">
                         <span>Eliminar</span>
                     </a>
                </div>
                <h3 class="m_3">Nombre Usuario</h3>
          </div>
          <div class="col-md-4 grid_1 wow lightSpeedIn" data-wow-delay="0.4s"> 
          	 <img src="images/pic5.jpg" class="img-responsive" alt=""/>
          	    <div class="btn-wrap bg_2">
                     <a href="#" class="text_3 color_3">
                         <span>Editar</span>
                     </a>
					 <a href="#" class="text_3 color_3 deleteuser">
                         <span>Eliminar</span>
                     </a>
                </div>
                <h3 class="m_3">Nombre Usuario</h3>
          </div>
          <div class="col-md-4 grid_1 wow lightSpeedIn" data-wow-delay="0.4s"> 
          	 <img src="images/pic5.jpg" class="img-responsive" alt=""/>
          	    <div class="btn-wrap bg_2">
                     <a href="#" class="text_3 color_3">
                         <span>Editar</span>
                     </a>
					 <a href="#" class="text_3 color_3 deleteuser">
                         <span>Eliminar</span>
                     </a>
                </div>
                <h3 class="m_3">Nombre Usuario</h3>
          </div>
          <div class="clearfix"> </div>
       </div>
       <div class="profile_button wow rotateInUpLeft" data-wow-delay="0.4s">
	     <a href="#" class="btn1 btn4 btn-1 btn1-1b">Agregar usuario</a>
       </div>
	</div>
</div>
</div>
<div class='profiles'>
	<div class="container">
	  <div class="about_box1">
	  	<h1 class="m_4 wow flipInX" data-wow-delay="0.4s">
          Administradores
        </h1>
        <p class="m_5 wow flipInY" data-wow-delay="0.4s">
           Agrega, modifica o elimina administradores
        </p>
       <div class="about_grid1">
          <div class="col-md-4 grid_1 wow lightSpeedIn" data-wow-delay="0.4s"> 
          	<p class="m_3">Nombre Admin</p>
          	    <div class="btn-wrap bg_2">
                     <a href="#" class="text_3 color_3">
                         <span>Editar</span>
                     </a>
					 <a href="#" class="text_3 color_3">
                         <span>Eliminar</span>
                     </a>
                </div>
          </div>
          <div class="clearfix"> </div>
       </div>
       <div class="profile_button wow rotateInUpLeft" data-wow-delay="0.4s">
	     <a href="#" class="btn1 btn4 btn-1 btn1-1b">Agregar Administrador</a>
       </div>
	</div>
</div>
</div>
<?php
	$footer = file_get_contents('footer.php');
	echo $footer;
?>
</body>
</html>		